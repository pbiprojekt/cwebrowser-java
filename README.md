**Pracujcie w PowerShellu!**
## Co trzeba zainstalowac: ##
	node.js (potrzebny do angulara 2)
	npm (menadzer pakietow - angular2)
	angular-cli
	ng-bootstrap
	jquery
	tether
Powyższe dostępne pod tym linkiem: https://docs.npmjs.com/getting-started/installing-node

Reszte za pomocą komend:

```
#!bash

npm install -g angular-cli
npm install --save @ng-bootstrap/ng-bootstrap
npm install jquery
npm install -g gulp
npm install tether
npm install ng2-page-scroll --save
```


Ogolnie ng-cli ma bardzo duzo fajnych rzeczy, np tworzenie dyrektyw czy klas, uruchamianie testow itp. z poziomu powershella.
Wiecej info tu: https://github.com/angular/angular-cli/blob/master/README.md
	
## Uruchamianie angulara: ##
	1. Wchodzisz w folder projektu
	2. Odpalasz w PowerShellu komende "npm start"
	3. Strona dostępna jest pod http://localhost:4200/

## Gdzie w projekcie sa pliki angulara? ##
	/src/main/javascript
## Jak korzystać z bootstrapa? ##
	Używając dyrektyw: https://ng-bootstrap.github.io/#/components

## Komunikacja z backendem: ##
Ustawiłem proxy na backend (standardowy url springa + /app: http://localhost:8080/app).
Używając http://localhost:4200/api w angularze odwołasz się do backendu. Proxy znajduje się w pliku proxy.conf.json.

## Jak uruchomić serwer Springa ##
Odpalić maina z klasy MainApp