package cwebrowser.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cwebrowser.model.Weakness;
import cwebrowser.model.container.ModelContainer;

@Controller
public class HomeController {

	ModelContainer container;

	@Autowired
	public HomeController(ModelContainer container) {
		this.container = container;
	}
	
	
	@RequestMapping("/")
	@ResponseBody
	String home() {
		StringBuffer buffer = new StringBuffer();
		for(Weakness w : container.getWeaknesses()){
			buffer.append(w.getName());
			buffer.append("<br>");
		}
		return buffer.toString();
		
	}
}
