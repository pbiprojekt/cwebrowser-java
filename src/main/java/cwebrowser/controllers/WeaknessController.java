package cwebrowser.controllers;

import java.math.BigInteger;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cwebrowser.model.Weakness;
import cwebrowser.model.WeaknessInfo;
import cwebrowser.model.container.ModelContainer;

@RestController
public class WeaknessController {

	private ModelContainer container;
	private List<WeaknessInfo> weaknsesses;

	@Autowired
	public WeaknessController(ModelContainer container) {
		this.container = container;
	}

	@GetMapping("/app/weaknesses")
	public List<WeaknessInfo> weaknesses() {
		if (weaknsesses == null) {
			weaknsesses = container.getWeaknesses().stream().map(w -> new WeaknessInfo(w.getName(), w.getID()))
					.sorted((w1, w2) -> w1.getName().compareTo(w2.getName())).collect(Collectors.toList());
		}
		return weaknsesses;
	}

	@GetMapping(value = "/app/weakness/{weakness_id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity weakness(@PathVariable("weakness_id") String weaknessId) {
		BigInteger weaknessBiId = new BigInteger(weaknessId);
		try {
			return new ResponseEntity(container.getWeaknesses().stream().filter(w -> {
				return w.getID().equals(weaknessBiId);
			}).findFirst().get(), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}
}
