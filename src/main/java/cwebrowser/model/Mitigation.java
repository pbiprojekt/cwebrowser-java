//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.12.16 at 07:01:09 PM CET 
//


package cwebrowser.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Mitigation_Phase" type="{}SDLC_Phase_Type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Mitigation_Strategy" maxOccurs="unbounded" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;whiteSpace value="collapse"/>
 *               &lt;enumeration value="Compilation or Build Hardening"/>
 *               &lt;enumeration value="Enforcement by Conversion"/>
 *               &lt;enumeration value="Environment Hardening"/>
 *               &lt;enumeration value="Firewall"/>
 *               &lt;enumeration value="Identify and Reduce Attack Surface"/>
 *               &lt;enumeration value="Input Validation"/>
 *               &lt;enumeration value="Language Selection"/>
 *               &lt;enumeration value="Libraries or Frameworks"/>
 *               &lt;enumeration value="Limit Resource Consumption"/>
 *               &lt;enumeration value="Output Encoding"/>
 *               &lt;enumeration value="Parameterization"/>
 *               &lt;enumeration value="Refactoring"/>
 *               &lt;enumeration value="Sandbox or Jail"/>
 *               &lt;enumeration value="Separation of Privilege"/>
 *               &lt;enumeration value="Threat Modeling"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Applicable_Languages" type="{}Languages_List_Type" minOccurs="0"/>
 *         &lt;element name="Mitigation_Description" type="{}Structured_Text_Type" minOccurs="0"/>
 *         &lt;element name="Mitigation_Effectiveness" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="None"/>
 *               &lt;enumeration value="Defense in Depth"/>
 *               &lt;enumeration value="Limited"/>
 *               &lt;enumeration value="Incidental"/>
 *               &lt;enumeration value="Moderate"/>
 *               &lt;enumeration value="High"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Mitigation_Effectiveness_Notes" type="{}Structured_Text_Type" minOccurs="0"/>
 *         &lt;element name="SubMitigations" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{}Mitigation" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="References" type="{}Reference_List_Type" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Mitigation_ID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mitigationPhases",
    "mitigationStrategies",
    "applicableLanguages",
    "mitigationDescription",
    "mitigationEffectiveness",
    "mitigationEffectivenessNotes",
    "subMitigations",
    "references"
})
@XmlRootElement(name = "Mitigation")
public class Mitigation
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    @XmlElement(name = "Mitigation_Phase")
    protected List<SDLCPhaseType> mitigationPhases;
    @XmlElement(name = "Mitigation_Strategy")
    protected List<String> mitigationStrategies;
    @XmlElement(name = "Applicable_Languages")
    protected LanguagesListType applicableLanguages;
    @XmlElement(name = "Mitigation_Description")
    protected StructuredTextType mitigationDescription;
    @XmlElement(name = "Mitigation_Effectiveness")
    protected String mitigationEffectiveness;
    @XmlElement(name = "Mitigation_Effectiveness_Notes")
    protected StructuredTextType mitigationEffectivenessNotes;
    @XmlElement(name = "SubMitigations")
    protected Mitigation.SubMitigations subMitigations;
    @XmlElement(name = "References")
    protected ReferenceListType references;
    @XmlAttribute(name = "Mitigation_ID")
    protected String mitigationID;

    /**
     * Gets the value of the mitigationPhases property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mitigationPhases property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMitigationPhases().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDLCPhaseType }
     * 
     * 
     */
    public List<SDLCPhaseType> getMitigationPhases() {
        if (mitigationPhases == null) {
            mitigationPhases = new ArrayList<SDLCPhaseType>();
        }
        return this.mitigationPhases;
    }

    /**
     * Gets the value of the mitigationStrategies property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mitigationStrategies property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMitigationStrategies().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMitigationStrategies() {
        if (mitigationStrategies == null) {
            mitigationStrategies = new ArrayList<String>();
        }
        return this.mitigationStrategies;
    }

    /**
     * Gets the value of the applicableLanguages property.
     * 
     * @return
     *     possible object is
     *     {@link LanguagesListType }
     *     
     */
    public LanguagesListType getApplicableLanguages() {
        return applicableLanguages;
    }

    /**
     * Sets the value of the applicableLanguages property.
     * 
     * @param value
     *     allowed object is
     *     {@link LanguagesListType }
     *     
     */
    public void setApplicableLanguages(LanguagesListType value) {
        this.applicableLanguages = value;
    }

    /**
     * Gets the value of the mitigationDescription property.
     * 
     * @return
     *     possible object is
     *     {@link StructuredTextType }
     *     
     */
    public StructuredTextType getMitigationDescription() {
        return mitigationDescription;
    }

    /**
     * Sets the value of the mitigationDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuredTextType }
     *     
     */
    public void setMitigationDescription(StructuredTextType value) {
        this.mitigationDescription = value;
    }

    /**
     * Gets the value of the mitigationEffectiveness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMitigationEffectiveness() {
        return mitigationEffectiveness;
    }

    /**
     * Sets the value of the mitigationEffectiveness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMitigationEffectiveness(String value) {
        this.mitigationEffectiveness = value;
    }

    /**
     * Gets the value of the mitigationEffectivenessNotes property.
     * 
     * @return
     *     possible object is
     *     {@link StructuredTextType }
     *     
     */
    public StructuredTextType getMitigationEffectivenessNotes() {
        return mitigationEffectivenessNotes;
    }

    /**
     * Sets the value of the mitigationEffectivenessNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuredTextType }
     *     
     */
    public void setMitigationEffectivenessNotes(StructuredTextType value) {
        this.mitigationEffectivenessNotes = value;
    }

    /**
     * Gets the value of the subMitigations property.
     * 
     * @return
     *     possible object is
     *     {@link Mitigation.SubMitigations }
     *     
     */
    public Mitigation.SubMitigations getSubMitigations() {
        return subMitigations;
    }

    /**
     * Sets the value of the subMitigations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Mitigation.SubMitigations }
     *     
     */
    public void setSubMitigations(Mitigation.SubMitigations value) {
        this.subMitigations = value;
    }

    /**
     * Gets the value of the references property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceListType }
     *     
     */
    public ReferenceListType getReferences() {
        return references;
    }

    /**
     * Sets the value of the references property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceListType }
     *     
     */
    public void setReferences(ReferenceListType value) {
        this.references = value;
    }

    /**
     * Gets the value of the mitigationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMitigationID() {
        return mitigationID;
    }

    /**
     * Sets the value of the mitigationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMitigationID(String value) {
        this.mitigationID = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{}Mitigation" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mitigations"
    })
    public static class SubMitigations
        implements Serializable
    {

        private final static long serialVersionUID = -1L;
        @XmlElement(name = "Mitigation", required = true)
        protected List<Mitigation> mitigations;

        /**
         * Gets the value of the mitigations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the mitigations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMitigations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Mitigation }
         * 
         * 
         */
        public List<Mitigation> getMitigations() {
            if (mitigations == null) {
                mitigations = new ArrayList<Mitigation>();
            }
            return this.mitigations;
        }

    }

}
