//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.12.16 at 07:01:09 PM CET 
//


package cwebrowser.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Views">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{}View" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Categories">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{}Category" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Weaknesses">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{}Weakness" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Compound_Elements">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{}Compound_Element" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="Catalog_Name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Catalog_Version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Catalog_Date" type="{http://www.w3.org/2001/XMLSchema}date" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "views",
    "categories",
    "weaknesses",
    "compoundElements"
})
@XmlRootElement(name = "Weakness_Catalog")
public class WeaknessCatalog
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    @XmlElement(name = "Views", required = true)
    protected WeaknessCatalog.Views views;
    @XmlElement(name = "Categories", required = true)
    protected WeaknessCatalog.Categories categories;
    @XmlElement(name = "Weaknesses", required = true)
    protected WeaknessCatalog.Weaknesses weaknesses;
    @XmlElement(name = "Compound_Elements", required = true)
    protected WeaknessCatalog.CompoundElements compoundElements;
    @XmlAttribute(name = "Catalog_Name", required = true)
    protected String catalogName;
    @XmlAttribute(name = "Catalog_Version", required = true)
    protected String catalogVersion;
    @XmlAttribute(name = "Catalog_Date")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar catalogDate;

    /**
     * Gets the value of the views property.
     * 
     * @return
     *     possible object is
     *     {@link WeaknessCatalog.Views }
     *     
     */
    public WeaknessCatalog.Views getViews() {
        return views;
    }

    /**
     * Sets the value of the views property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeaknessCatalog.Views }
     *     
     */
    public void setViews(WeaknessCatalog.Views value) {
        this.views = value;
    }

    /**
     * Gets the value of the categories property.
     * 
     * @return
     *     possible object is
     *     {@link WeaknessCatalog.Categories }
     *     
     */
    public WeaknessCatalog.Categories getCategories() {
        return categories;
    }

    /**
     * Sets the value of the categories property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeaknessCatalog.Categories }
     *     
     */
    public void setCategories(WeaknessCatalog.Categories value) {
        this.categories = value;
    }

    /**
     * Gets the value of the weaknesses property.
     * 
     * @return
     *     possible object is
     *     {@link WeaknessCatalog.Weaknesses }
     *     
     */
    public WeaknessCatalog.Weaknesses getWeaknesses() {
        return weaknesses;
    }

    /**
     * Sets the value of the weaknesses property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeaknessCatalog.Weaknesses }
     *     
     */
    public void setWeaknesses(WeaknessCatalog.Weaknesses value) {
        this.weaknesses = value;
    }

    /**
     * Gets the value of the compoundElements property.
     * 
     * @return
     *     possible object is
     *     {@link WeaknessCatalog.CompoundElements }
     *     
     */
    public WeaknessCatalog.CompoundElements getCompoundElements() {
        return compoundElements;
    }

    /**
     * Sets the value of the compoundElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeaknessCatalog.CompoundElements }
     *     
     */
    public void setCompoundElements(WeaknessCatalog.CompoundElements value) {
        this.compoundElements = value;
    }

    /**
     * Gets the value of the catalogName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalogName() {
        return catalogName;
    }

    /**
     * Sets the value of the catalogName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalogName(String value) {
        this.catalogName = value;
    }

    /**
     * Gets the value of the catalogVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalogVersion() {
        return catalogVersion;
    }

    /**
     * Sets the value of the catalogVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalogVersion(String value) {
        this.catalogVersion = value;
    }

    /**
     * Gets the value of the catalogDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCatalogDate() {
        return catalogDate;
    }

    /**
     * Sets the value of the catalogDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCatalogDate(XMLGregorianCalendar value) {
        this.catalogDate = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{}Category" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "categories"
    })
    public static class Categories
        implements Serializable
    {

        private final static long serialVersionUID = -1L;
        @XmlElement(name = "Category")
        protected List<Category> categories;

        /**
         * Gets the value of the categories property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the categories property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCategories().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Category }
         * 
         * 
         */
        public List<Category> getCategories() {
            if (categories == null) {
                categories = new ArrayList<Category>();
            }
            return this.categories;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{}Compound_Element" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "compoundElements"
    })
    public static class CompoundElements
        implements Serializable
    {

        private final static long serialVersionUID = -1L;
        @XmlElement(name = "Compound_Element")
        protected List<CompoundElement> compoundElements;

        /**
         * Gets the value of the compoundElements property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the compoundElements property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCompoundElements().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CompoundElement }
         * 
         * 
         */
        public List<CompoundElement> getCompoundElements() {
            if (compoundElements == null) {
                compoundElements = new ArrayList<CompoundElement>();
            }
            return this.compoundElements;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{}View" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "views"
    })
    public static class Views
        implements Serializable
    {

        private final static long serialVersionUID = -1L;
        @XmlElement(name = "View")
        protected List<View> views;

        /**
         * Gets the value of the views property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the views property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getViews().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link View }
         * 
         * 
         */
        public List<View> getViews() {
            if (views == null) {
                views = new ArrayList<View>();
            }
            return this.views;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{}Weakness" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "weaknesses"
    })
    public static class Weaknesses
        implements Serializable
    {

        private final static long serialVersionUID = -1L;
        @XmlElement(name = "Weakness")
        protected List<Weakness> weaknesses;

        /**
         * Gets the value of the weaknesses property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the weaknesses property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getWeaknesses().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Weakness }
         * 
         * 
         */
        public List<Weakness> getWeaknesses() {
            if (weaknesses == null) {
                weaknesses = new ArrayList<Weakness>();
            }
            return this.weaknesses;
        }

    }

}
