package cwebrowser.model;

import java.math.BigInteger;

public class WeaknessInfo {

	private String name;
	private BigInteger id;
	
	
	
	public WeaknessInfo(String name, BigInteger id) {
		super();
		this.name = name;
		this.id = id;
	}

	public BigInteger getId() {
		return id;
	}
	
	public void setId(BigInteger id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
