package cwebrowser.model.container;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Component;

import cwebrowser.model.Weakness;
import cwebrowser.model.WeaknessCatalog;

@Component
public class ModelContainer {

	private WeaknessCatalog catalog;

	public ModelContainer() {
		JAXBContext context;
		try {
			context = JAXBContext.newInstance(WeaknessCatalog.class);
			File f = new File("src/main/resources/cwec_v2.9.xml");
			Unmarshaller u = context.createUnmarshaller();
			catalog = (WeaknessCatalog) u.unmarshal(f);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	public List<Weakness> getWeaknesses(){
		return catalog.getWeaknesses().getWeaknesses();
	}
}
