import {Component, trigger, state, style, transition, animate} from '@angular/core';
import {WeaknessesService} from './weaknesses/weaknesses.service'
import {SimplifiedWeakness} from "./weakness/simplified-weakness";
import {Router, RoutesRecognized} from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('searchResultAnimation', [
      state('in', style({height: '28px', opacity: '1'})),
      transition('void => *', [
        style({height: '0px', opacity: '0'}),
        animate(500)
      ]),
      transition('* => void', [
        animate(500, style({height: '0px', opacity: '0'}))
      ])
    ])
  ]
})

export class AppComponent {

  weaknesses: SimplifiedWeakness[];
  private navExpanded: boolean = false;
  searchTerm: string = "";
  focused: boolean = false;
  mouseover: boolean = false;

  constructor(private weaknessesService: WeaknessesService, private router: Router) {
    this.weaknesses = weaknessesService.weaknesses.getValue();
    weaknessesService.weaknesses.asObservable().subscribe(weaknesses => this.weaknesses = weaknesses);
    router.events.filter(event => event instanceof RoutesRecognized).subscribe((event: RoutesRecognized) => {
      if (event.urlAfterRedirects == "/starter") {
        this.navExpanded = false;
      }
    });
  };


  public toggleNav(): void {
    this.navExpanded = !this.navExpanded;
  }

}
