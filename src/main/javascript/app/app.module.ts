import {BrowserModule} from '@angular/platform-browser'
import {NgModule} from '@angular/core'
import {FormsModule} from '@angular/forms'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'
import {HttpModule, JsonpModule} from '@angular/http'
import {WeaknessesListComponent} from './weaknesses/weaknesses-list.component'
import {WeaknessComponent} from './weakness/weakness.component'
import {SearchWeaknessFilter} from './shared/search-weakness.filter'
import {LimitFilter} from './shared/limit.filter'
import {AppComponent} from './app.component'
import {WeaknessesService} from './weaknesses/weaknesses.service'
import {RouterModule, Routes} from '@angular/router'
import {PageNotFoundComponent} from "./shared/page-not-found.component";
import {Ng2PageScrollModule} from 'ng2-page-scroll';
import {StarterComponent} from "./shared/starter.component";


const appRoutes: Routes = [
  {path: 'weakness/:id', component: WeaknessComponent},
  {
    path: 'weakness',
    redirectTo: '/starter'
  },
  {path: 'starter', component: StarterComponent},
  {
    path: '',
    redirectTo: '/starter',
    pathMatch: 'full'
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    WeaknessesListComponent,
    WeaknessComponent,
    PageNotFoundComponent,
    SearchWeaknessFilter,
    LimitFilter,
    StarterComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule.forRoot(appRoutes),
    Ng2PageScrollModule.forRoot()
  ],
  providers: [WeaknessesService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
