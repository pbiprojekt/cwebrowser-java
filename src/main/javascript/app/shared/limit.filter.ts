import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'limit'
})
export class LimitFilter implements PipeTransform {
  transform(objects: any[], limit: number): any {
    return objects.filter((element, index) => index < limit);
  }
}
