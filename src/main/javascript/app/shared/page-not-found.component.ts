import {Component} from "@angular/core";

@Component({
  selector: 'page-not-found',
  styles:  ['h1 { text-align: center; }'],
  template: `
  <h1>Page not found</h1>`
})

export class PageNotFoundComponent {

}
