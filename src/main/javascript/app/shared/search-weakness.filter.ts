import {Pipe, PipeTransform, Inject} from "@angular/core";
import {SimplifiedWeakness} from "../weakness/simplified-weakness";
import {AppComponent} from "../app.component";

@Pipe({
  name: 'searchWeakness'
})
export class SearchWeaknessFilter implements PipeTransform {
  constructor(@Inject(AppComponent) private comp: AppComponent) {

  }

  transform(weaknesses: SimplifiedWeakness[], searchTerm: string): any {
    let result = weaknesses.filter(weakness =>
    weakness.name.toUpperCase().includes(searchTerm.toUpperCase()) || weakness.id.toString().startsWith(searchTerm)).map(weakness => {
      weakness.modifiedName = weakness.name
      weakness.modifiedId = weakness.id;
      let indexOf = weakness.name.toUpperCase().indexOf(searchTerm.toUpperCase());
      if (indexOf >= 0) {
        let substr1 = weakness.name.substring(0, indexOf);
        let substr2 = weakness.name.substring(indexOf, indexOf + searchTerm.length);
        let substr3 = weakness.name.substring(indexOf + searchTerm.length);
        weakness.modifiedName = substr1.concat('<span class="highlight-found">', substr2, '</span>', substr3);
      } else {
        let indexOfId = weakness.id.toString().indexOf(searchTerm);
        if (indexOfId >= 0) {
          let substr1 = weakness.id.toString().substring(0, indexOfId);
          let substr2 = weakness.id.toString().substring(indexOfId, indexOfId + searchTerm.length);
          let substr3 = weakness.id.toString().substring(indexOfId + searchTerm.length);
          weakness.modifiedId = substr1.concat('<span class="highlight-found">', substr2, '</span>', substr3);
        }
      }
      return weakness;
    });
    return result;
  }
}
