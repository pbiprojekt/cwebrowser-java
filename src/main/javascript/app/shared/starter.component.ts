import {Component} from "@angular/core";

@Component({
  selector: 'starter',
  styles: ['h1 { text-align: center; }'],
  template: `
  <div class="container">
    <h3>Common Weakness Enumeration</h3>
    <p>CWE™ International in scope and free for public use, CWE provides a unified, measurable set of software weaknesses that is enabling more effective discussion, description, selection, and use of software security tools and services that can find these weaknesses in source code and operational systems as well as better understanding and management of software weaknesses related to architecture and design.</p>
    <weaknesses-list></weaknesses-list>
  </div>
`
})

export class StarterComponent {

}
