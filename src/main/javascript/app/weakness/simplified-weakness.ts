export class SimplifiedWeakness {
  public modifiedName: string;
  public modifiedId: string;

  constructor(public name: string, public id: string) {
    this.modifiedName = name;
    this.modifiedId = id;
  }
}
