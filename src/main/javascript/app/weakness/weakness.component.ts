import {Component, trigger, state, style, transition, animate} from '@angular/core'
import {WeaknessesService} from '../weaknesses/weaknesses.service'
import {ActivatedRoute, Params, Router} from '@angular/router'
declare var $: any;
declare var hljs: any;

@Component({
  selector: 'weakness',
  templateUrl: './weakness.component.html',
  styleUrls: ['./weakness.component.css'],
  animations: [
    trigger('animated', [
      state('in', style({opacity: '1'})),
      transition('void => *', [
        style({opacity: '0'}),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({opacity: '0'}))
      ])
    ])
  ]
})

export class WeaknessComponent {
  selectedWeakness: JSON;
  private entityMap: Object = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };
  private weaknessChanged: boolean = false;


  constructor(private weaknessesService: WeaknessesService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => {
        this.selectedWeakness = null;
        return this.weaknessesService.getFullWeakness(params['id']);
      })
      .subscribe(weakness => {
        this.selectedWeakness = weakness;
        this.weaknessChanged = true;
        console.debug(weakness);
      }, error => {
        console.error(error);
        this.router.navigate(['/page-not-found'])
      });
  }

  public updateHighlighting(): void {
    if (this.weaknessChanged) {
      this.weaknessChanged = false;
      console.debug("updating highlighting");
      $('pre code').each(function (i, block) {
        hljs.highlightBlock(block);
      });
    }
  }

  public normalizeUnderscored(text: string): string {
    return text.toString().replace(/_/g, " ").toLowerCase().replace(/^./, function (str) {
      return str.toUpperCase();
    }).replace(/ ./g, function (str) {
      return str.toUpperCase();
    })
  }

  public escapeHTML(string: string): string {
    let entityMap = this.entityMap;
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
      return entityMap[s];
    });
  }

  public unpackCode(array: any[]): string {
    if (!array) {
      return "";
    }
    let text = "";
    for (let i = 0; i < array.length; i++) {
      if (array[i].name == "Code_Example_Language") {
        continue;
      } else if (array[i].value) {
        text += this.escapeHTML(array[i].value) + "<br />";
      } else {
        text += this.unpackCode(array[i].textTitlesAndTextsAndCodeExampleLanguages);
      }
    }
    return text;
  }

}
