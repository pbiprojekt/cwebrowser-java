import {SimplifiedWeakness} from '../weakness/simplified-weakness'
import {WeaknessesService} from './weaknesses.service'
import {Component} from '@angular/core'
import {WeaknessesTableSort} from './weaknesses-table-sort.enum'
import {Router} from '@angular/router';


@Component({
  selector: 'weaknesses-list',
  templateUrl: './weaknesses-list.component.html',
  styleUrls: ['./weaknesses-list.component.css']
})
export class WeaknessesListComponent {
  weaknesses: SimplifiedWeakness[];
  loading: boolean = true;

  constructor(private weaknessesService: WeaknessesService, private router: Router) {
    this.weaknesses = weaknessesService.weaknesses.getValue();
    if (this.weaknesses.length === 0) {
      this.loading = true;
    } else {
      this.loading = false;
    }
    weaknessesService.weaknesses.asObservable().subscribe(weaknesses => {
      this.loading = false;
      this.weaknesses = weaknesses;
    });
  };

  public isCurrentWeakness(weakness: SimplifiedWeakness): boolean {
    return this.router.url === "/weakness/" + weakness.id;
  }

  public sortById(): void {
    if (this.weaknessesService.howSorted === WeaknessesTableSort.BY_ID_ASC) this.sortWeaknesses(WeaknessesTableSort.BY_ID_DESC);
    else this.sortWeaknesses(WeaknessesTableSort.BY_ID_ASC);
  }

  public sortByName(): void {
    if (this.weaknessesService.howSorted === WeaknessesTableSort.BY_NAME_ASC) this.sortWeaknesses(WeaknessesTableSort.BY_NAME_DESC);
    else this.sortWeaknesses(WeaknessesTableSort.BY_NAME_ASC);
  }

  private sortWeaknesses(howToSort: WeaknessesTableSort): void {
    switch (howToSort) {
      case WeaknessesTableSort.BY_ID_ASC:
        this.weaknesses.sort((w1, w2) => Number.parseInt(w1.id) - Number.parseInt(w2.id))
        break;
      case WeaknessesTableSort.BY_ID_DESC:
        this.weaknesses.sort((w2, w1) => Number.parseInt(w1.id) - Number.parseInt(w2.id))
        break;
      case WeaknessesTableSort.BY_NAME_ASC:
        this.weaknesses.sort((w1, w2) => w1.name.localeCompare(w2.name));
        break;
      case WeaknessesTableSort.BY_NAME_DESC:
        this.weaknesses.sort((w2, w1) => w1.name.localeCompare(w2.name));
        break;
      default:
        console.error("Błędny parametr sortowania: " + howToSort);
        break;
    }
    this.weaknessesService.howSorted = howToSort;
  }

}
