export enum WeaknessesTableSort{
  BY_NAME_ASC = 2,
  BY_NAME_DESC = 3,
  BY_ID_ASC = 4,
  BY_ID_DESC = 5
}
