import {SimplifiedWeakness} from '../weakness/simplified-weakness'
import {Injectable} from '@angular/core'
import {Http, Response} from '@angular/http'
import {Observable} from 'rxjs/Observable'
import {BehaviorSubject}    from 'rxjs/BehaviorSubject';
import '../shared/rxjs-operators';
import {WeaknessesTableSort} from "./weaknesses-table-sort.enum";

@Injectable()
export class WeaknessesService {
  private weaknessesUrl: string = 'http://localhost:4200/app/weaknesses';
  private weaknessUrl: string = 'http://localhost:4200/app/weakness/';
  weaknesses: BehaviorSubject<SimplifiedWeakness[]> = new BehaviorSubject<SimplifiedWeakness[]>(new Array<SimplifiedWeakness>());
  howSorted: WeaknessesTableSort = WeaknessesTableSort.BY_NAME_ASC;

  constructor(private http: Http) {
    this.getSimplifieldWeaknesses();
  };

  getSimplifieldWeaknesses(): void {
    console.debug("Rozpoczynam pobieranie uproszczonych obiektów podatności.");
    this.http.get(this.weaknessesUrl).map(this.extractData).catch(this.handleError).subscribe(
      weaknesses => {
        console.debug("Pobralem uproszczone obiekty podatnosci.");
        this.weaknesses.next(weaknesses);
      },
      error => {
        console.error(error);
      });
  }

  getFullWeakness(id: string): Observable<JSON> {
    console.debug("Rozpoczynam pobieranie podatnosci o id " + id);
    return this.http.get(this.weaknessUrl + id).map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response) {
    return res.json();
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
