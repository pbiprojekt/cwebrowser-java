import { CWEBrowserPage } from './app.po';

describe('cwebrowser App', function() {
  let page: CWEBrowserPage;

  beforeEach(() => {
    page = new CWEBrowserPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
